
Praca domowa #4
===============

Korzystając z jednej z poznanych na wykładzie metod zwiększyć wydajność swojego serwera HTTP.
Podczas oceniania proszę porównać wydajność swojego serwera i kolegów oraz przyznać punkty
według poniższego algorytmu:

* 3 pkt. dla najszybszego serwera
* 2 pkt. dla serwera w drugiej kolejności
* po 1 pkt. dla pozostałych serwerów

Do porównania proszę użyć stworzonego na laboratoriach programu (z ew. poprawkami), a wyniki testów,
po umieszczeniu w sieci, dołączyć jako komentarz do oceny w postaci linka.