__author__ = 'G3nocide'
# -*- encoding: utf-8 -*-

import socket

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler
import logging
from daemon import runner

import os.path

logger = logging.getLogger("DemonLog")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/home/p17/lab/tmp/daemon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)
_PATH = 'home/p17/lab/web'

def print_file(path, extension):
    html = ''

    ctype_jpg = "image/jpeg"
    ctype_png = "image/png"
    ctype_html = "text/html"
    ctype_plain = "text/plain"

    if extension == '.jpg':
        html_content_type = ctype_jpg
    if extension == '.png':
        html_content_type = ctype_png
    if extension == '.html':
        html_content_type = ctype_html
    if extension == '.txt':
        html_content_type = ctype_plain



    if extension in ('.txt', '.html', '.jpg', '.png'):
        html = "<html><body>"
        html += "<meta http-equiv='Content-Type' content='" + html_content_type + "; charset=utf-8'>"

        if extension in (".txt", ".html"):
            try:
                txt=open(_PATH + path).read()
            except:
                logger.debug("File doesn't exist")
                return "<html><body>File doesn't exist</body></html> " + _PATH + " " + path
            html+=txt
        if extension in (".jpg", ".png"):
            try:
                data_uri = open(_PATH + path, 'rb').read().encode('base64').replace('\n', '')
            except:
                logger.debug("File doesn't exist")
                return "<html><body>File doesn't exist</body></html>"

            html+= '<img src="data:image/png;base64,{0}"/>'.format(data_uri)
        html+="</body></html>"
    else:
        html = "<html><body>Not supported file extension</body></html>"
    return html

def list_directories(path,r_uri):
    html_dir_list = '<html><body>'
    dir = ''
    splitted_path = str.split(path, "/")
    try:
        elem_list = os.listdir(path)
    except:

        logger.debug("Directory doesn't exist")
        return '<html><body><meta http-equiv="Content-Type" content="text/html"; charset="utf-8">Directory doesn"t exist</body></html>'
    if splitted_path.__len__() > 1:
        for dir_elem in splitted_path:
            if(dir_elem != 'web'):
                dir+='/'+dir_elem
    if elem_list.__len__() != 0:
        for elem in elem_list:
            html_dir_list+='<div><a href="'+ r_uri +'/'+ elem + '">' + elem + '</a></div>'
    else:
        html_dir_list += "<div>{empty}</div>"
    return html_dir_list + '</body></html>'



class my_serv():

    def get_request(self, server_socket):
        # Czekanie na połączenie
        request = '';
            # Odebranie żądania

            #while True:
                #data = connection.recv(1024)
                #request += data

                #if not data:
                    #break
        request = server_socket.recv(1024)
        if request:
            header_200 = "HTTP/1.1 200 OK"


            get_req_lines = str.split(request, "\r\n")
            get_uri_line = str.split(get_req_lines[0], " ");

            html_response = ''

            if get_uri_line[0].upper() == 'GET' and "HTTP" in get_uri_line[2].upper():
                r_uri = get_uri_line[1]
                if  get_uri_line[1] != '/favicon.ico':
                    if get_uri_line[1] == '/':
                        html_response += header_200 + "\r\n\r\n" +  list_directories(_PATH,'')
                    else:
                        fileName, fileExtension = os.path.splitext(get_uri_line[1])
                        if fileExtension == '':
                            r_uri = get_uri_line[1]
                            html_response += header_200 + "\r\n\r\n" +  list_directories(_PATH+get_uri_line[1],r_uri)
                        else:
                            r_uri = get_uri_line[1]
                            html_response += header_200 + "\r\n\r\n" + print_file(get_uri_line[1], fileExtension)

            else:
                resp_header = "HTTP/1.1 401 ERROR"
                html_response = resp_header + "\r\n\r\n" + "Wystąpił błąd"
            server_socket.sendall(html_response)
        server_socket.close()

class ServApp:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/home/p17/lab/Daemonn11.pid'
        self.pidfile_timeout = 5

    # Główny kod aplikacji
    def run(self):
        server = MyServer(('194.29.175.240', 31017), MyHandler)
        logger.debug('App start')
        try:
            server.serve_forever()
        except:
            logger.error("Server error")

myServ = my_serv()

class MyHandler(BaseRequestHandler):
    def handle(self):
        myServ.get_request(self.request)

class MyServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1

if __name__ == '__main__':
    servApp = ServApp()
    daemon_runner = runner.DaemonRunner(servApp)
    daemon_runner.daemon_context.files_preserve = [handler.stream]
    daemon_runner.do_action()
