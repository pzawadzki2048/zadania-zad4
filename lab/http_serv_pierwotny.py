__author__ = 'G3nocide'
# -*- encoding: utf-8 -*-

import socket

from SocketServer import ThreadingMixIn, TCPServer, BaseRequestHandler
import logging
import time
from daemon import runner
import os.path


logger = logging.getLogger("DemonLog")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/tmp/demon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)



class App:
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/out'
        self.stderr_path = '/dev/err'
        self.pidfile_path = '/home/p14/dom/daemon100.pid'
        self.pidfile_timeout = 5

    # Główny kod aplikacji
    def run(self):
        while True:
            logger.info("App run")
            time.sleep(10)


def print_file(path, extension):
    html = ''
    print(extension)

    ctype_jpg = "image/jpeg"
    ctype_png = "image/png"
    ctype_html = "text/html"
    ctype_plain = "text/plain"

    if extension == '.jpg':
        html_content_type = ctype_jpg
    if extension == '.png':
        html_content_type = ctype_png
    if extension == '.html':
        html_content_type = ctype_html
    if extension == '.txt':
        html_content_type = ctype_plain



    if extension in ('.txt', '.html', '.jpg', '.png'):
        html = "<html><body>"
        html += "<meta http-equiv='Content-Type' content='" + html_content_type + "; charset=utf-8'>"

        if extension in (".txt", ".html"):
            try:
                txt=open('web' + path).read()
            except:
                return "<html><body>File doesn't exist</body></html>"
            html+=txt
        if extension in (".jpg", ".png"):
            try:
                data_uri = open('web' + path, 'rb').read().encode('base64').replace('\n', '')
            except:
                return "<html><body>File doesn't exist</body></html>"

            html+= '<img src="data:image/png;base64,{0}"/>'.format(data_uri)
        html+="</body></html>"
    else:
        html = "<html><body>Not supported file extension</body></html>"
    return html

def list_directories(path):
    html_dir_list = '<html><body>'
    dir = ''
    splitted_path = str.split(path, "/")
    try:
        elem_list = os.listdir(path)
    except:
        return '<html><body><meta http-equiv="Content-Type" content="text/html"; charset="utf-8">Podana ścieżka nie istnieje</body></html>'
    if splitted_path.__len__() > 1:
        for dir_elem in splitted_path:
            if(dir_elem != 'web'):
                dir+='/'+dir_elem
    if elem_list.__len__() != 0:
        for elem in elem_list:
            html_dir_list+='<div><a href="' + dir + '/' + elem + '">' + elem + '</a></div>'
    else:
        html_dir_list += "<div>{empty}</div>"
    return html_dir_list + '</body></html>'





def http_serve(self, server_socket):
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        request = '';
        try:
        # Odebranie żądania

        #while True:
        #data = connection.recv(1024)
        #request += data

        #if not data:
        #break
            request = connection.recv(1024)
            if request:
                header_404 = "HTTP/1.1 404 ERROR"
                header_200 = "HTTP/1.1 200 OK"


                get_req_lines = str.split(request, "\r\n")
                get_uri_line = str.split(get_req_lines[0], " ");

                html_response = ''

                if get_uri_line[0].upper() == 'GET' and "HTTP" in get_uri_line[2].upper():
                    print "Request:"
                    print request
                    if  get_uri_line[1] != '/favicon.ico':
                        if get_uri_line[1] == '/':
                            html_response += header_200 + "\r\n\r\n" +  list_directories('./web')
                        else:
                            fileName, fileExtension = os.path.splitext(get_uri_line[1])
                            if fileExtension == '':
                                print get_uri_line[1]
                                html_response += header_200 + "\r\n\r\n" +  list_directories('web' + get_uri_line[1])
                            else:
                                print get_uri_line[1]
                                html_response += header_200 + "\r\n\r\n" + print_file(get_uri_line[1], fileExtension)

                else:
                    resp_header = "HTTP/1.1 401 ERROR"
                    print resp_header
                    html_response = resp_header + "\r\n\r\n" + "Wystąpił błąd"
                print(html_response)
                try:
                    connection.sendall(html_response)
                except socket.error:
                    continue
        finally:
            # Zamknięcie połączenia
            connection.close()



class MyHandler(BaseRequestHandler):

    def handle(self):
        http_serve.handle_client(self.request)

class MyServer(ThreadingMixIn, TCPServer):
    allow_reuse_address = 1


if __name__ == '__main__':
    server = MyServer(('194.29.175.240', 31017), MyHandler)
    server.serve_forever()